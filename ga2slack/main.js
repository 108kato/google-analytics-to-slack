var spreadsheet = SpreadsheetApp.getActiveSpreadsheet();
var sheet = spreadsheet.getSheetByName('settings');
var today = new Date();
var oneWeekAgo = new Date(today.getTime() - 7 * 24 * 60 * 60 * 1000);
var yesterday = new Date(today.getTime() - 24 * 60 * 60 * 1000);
var startDate = Utilities.formatDate(yesterday, Session.getScriptTimeZone(), 'yyyy-MM-dd');
var reportBody = "";
/**
 * settingsのシートの view id 分処理
 */
function getViewID() {
    var lastRow = sheet.getLastRow();
    for (var i = 2; i <= lastRow; i++) {
        getGAreport(sheet.getRange(i, 2).getValue(), sheet.getRange(i, 1).getValue());

        if( i === lastRow ) {
            Logger.log("最後 = " + i);
            shapeBody(reportBody);
        }
    }
}
/**
 * Google Analyticsからレポート取得
 * @param id Google AnalyticsのVIEW ID
 * @param sitename サイト名
 */
function getGAreport(id, sitename) {
    var report = AnalyticsReporting.Reports.batchGet({
        "reportRequests": [
            {
                'viewId': id,
                'dateRanges': [
                    {
                        'startDate': startDate,
                        "endDate": startDate
                    }
                ],
                'metrics': [
                    {
                        'expression': "ga:users",
                        'alias': "ユーザー数"
                    },
                    {
                        'expression': "ga:newUsers",
                        'alias': "新規ユーザー数"
                    },
                    {
                        'expression': "ga:pageviews",
                        'alias': "ページビュー数"
                    },
                    {
                        'expression': "ga:sessions",
                        'alias': "セッション数"
                    }
                ]
            }
        ]
    });
    reportSummary(report, sitename);
}

/**
 * 現時点の解析結果をreportBodyに追加
 *
 * @param report レポート(取得結果)
 * @param site サイト名
 */
function reportSummary(report, site) {
    var text = "【" + site + "】" + "アナリティクス（" + startDate + "）" + "\n";
    var rep = report.reports[0];  // ベースとなるreports[]要素
    var data = rep.data;
    var dataNum = data.rowCount;  // データ行数

    if ( dataNum ) {
        var values = data.rows[0].metrics[0].values;
        var userCnt = values[0];
        var newuserCnt = values[1];
        var pvCnt = values[2];
        var sessionCnt = values[3];

        // ユーザー数
        if( userCnt != "" || userCnt != undefined ) {
            text += "ユーザー数 : " + userCnt + "\n";
        }else{
            text += "ユーザー数 : 0\n";
        }
        // 新規ユーザー数
        if( newuserCnt != "" || newuserCnt != undefined  ) {
            text += "新規ユーザー数 : " + newuserCnt + "\n";
        }else{
            text += "新規ユーザー数 : 0\n";
        }
        // セッション数
        if( sessionCnt != "" || sessionCnt != undefined ) {
            text += "セッション数 : " + sessionCnt + "\n";
        }else{
            text += "セッション数 : 0\n";
        }
        // ページビュー
        if( pvCnt != "" || pvCnt != undefined ) {
            text += "PV数 : " + pvCnt+ "\n";
        }else{
            text += "PV数 : 0\n";
        }
    }else if( dataNum === undefined ) {
        text += "ユーザー数 : 0\n";
        text += "新規ユーザー数 : 0\n";
        text += "セッション数 : 0\n";
        text += "PV数 : 0\n";
    }
    reportBody += text;
}
/**
 * 現時点の解析結果をSlackへポスト
 *
 * @param text reportBody
 */
function shapeBody(text) {
    if (text != "") {
        var payload = {
            "text": text,
            "channel": PropertiesService.getScriptProperties().getProperty("POST_CH_NAME") // Slackチャンネル名
        }
        postSlack(payload);
    }
}
/**
 * Slackポスト関数
 *
 * @param payload ポスト詳細
 */
function postSlack(payload) {
    var options = {
        "method" : "POST",
        "payload" : JSON.stringify(payload)
    }
    var url = profileId = PropertiesService.getScriptProperties().getProperty("SLACK_HOOK"); // SlackのWebHooks URL
    var response = UrlFetchApp.fetch(url, options);
    var content = response.getContentText("UTF-8");
}